﻿using NUnit.Framework;
using Rs.Web.ComponentBase.Models;
/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/24/2015 1:33:04 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rs.Web.ComponentBase.Dl
{
    [TestFixture]
    public class ItemRepositoryTest
    {
        private ItemRepository _itemRepository;
        [SetUp]
        public void Setup()
        {
            string connectionString = "Data Source=Dev-Win-12\\SqlExpress;Initial Catalog=AdBoard;User Id=rshrestha;Password=Passw0rd123;";
            _itemRepository = new ItemRepository(connectionString);
        }

        [Test]
        public void TestGetAll()
        {
            var itemList = _itemRepository.GetAll();

        }

        [Test]
        public void TestGetById()
        {
            var itemList = _itemRepository.Get(1);
        }

        [Test]
        public void TestAddItem()
        {
            var item = new Item
            {
                Description = "Added form utest",
                CategoryId = 1,
                Price = 0.0M,
                Title = "Test"
            };

            _itemRepository.Add(item);
        }

        [Test]
        public void TestUpdateItem()
        {
            var item = new Item
            {
                Id=3,
                Description = "Updated form utest",
                CategoryId = 1,
                Price = 0.0M,
                Title = "Test"
            };

            _itemRepository.Update(item);
        }
    }
}
