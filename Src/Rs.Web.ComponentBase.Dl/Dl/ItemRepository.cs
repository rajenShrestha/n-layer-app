﻿using Rs.Web.ComponentBase.Models;
/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/23/2015 11:56:57 PM
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rs.Web.ComponentBase.Dl
{
    public class ItemRepository : RepositoryBase, IRepository<Item>
    {
        public static readonly string TableName = "Items";
        public static readonly string IdColumn = "Id";
        public static readonly string TitleColumn = "Title";
        public static readonly string PriceColumn = "Price";
        public static readonly string DescriptionColumn = "Description";
        public static readonly string CategoryIdColumn = "CategoryId";

        public ItemRepository()
        {

        }

        public ItemRepository(string connectionString)
            : base(connectionString)
        {

        }

        public List<Item> GetAll()
        {
            var items = new List<Item>();
            string query = "Select * from " + TableName;
            SqlCommand cmd = new SqlCommand(query);
            var dataSet = FillDataSet(cmd, TableName);
            items = CreateItemList(dataSet);
            return items;
        }

        public List<Item> Get(long Id)
        {
            System.Threading.Thread.Sleep(1000);

            var items = new List<Item>();
            string query = "Select * from Items where CategoryId=" + Id;
            SqlCommand cmd = new SqlCommand(query);
            var dataSet = FillDataSet(cmd, TableName);
            items = CreateItemList(dataSet);
            return items;
        }

        public void Add(Item value)
        {
            string query = "Insert Into " + TableName
                + " (" + TitleColumn
                + "," + PriceColumn
                + "," + DescriptionColumn
                + "," + CategoryIdColumn + ") "
                + " Values(@Title, @Price, @Description, @CategoryId)";

            var con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Title", value.Title);
            cmd.Parameters.AddWithValue("@Price", value.Price);
            cmd.Parameters.AddWithValue("@Description", value.Description);
            cmd.Parameters.AddWithValue("@CategoryId", value.CategoryId);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        public void Update(Item value)
        {
            string query = "Update " + TableName
                + " SET " + TitleColumn + "=@Title"
                + "," + PriceColumn + "=@Price"
                + "," + DescriptionColumn + "=@Description"
                + "," + CategoryIdColumn + "=@CategoryId where Id=" + value.Id;

            var con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Title", value.Title);
            cmd.Parameters.AddWithValue("@Price", value.Price);
            cmd.Parameters.AddWithValue("@Description", value.Description);
            cmd.Parameters.AddWithValue("@CategoryId", value.CategoryId);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        private List<Item> CreateItemList(DataSet dataset)
        {
            var itemList = new List<Item>();
            foreach (DataRow dr in dataset.Tables[TableName].Rows)
            {
                itemList.Add(CreateItem(dr));
            }

            return itemList;
        }

        private Item CreateItem(DataRow itemTableRow)
        {
            var item = new Item
            {
                Id = (long)itemTableRow[IdColumn],
                Title = itemTableRow[TitleColumn] as string,
                Price = itemTableRow[PriceColumn] == null ? 0 : (decimal)itemTableRow[PriceColumn],
                Description = itemTableRow[DescriptionColumn] as string,
                CategoryId = itemTableRow[CategoryIdColumn] == null ? 0 : (long)itemTableRow[CategoryIdColumn]
            };

            return item;
        }
    }
}
