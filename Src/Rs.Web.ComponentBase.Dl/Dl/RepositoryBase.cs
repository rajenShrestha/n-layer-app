﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/23/2015 11:46:18 PM
*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rs.Web.ComponentBase.Dl
{
    public abstract class RepositoryBase
    {
        protected string _connectionString;

        public RepositoryBase()
        {

        }

        public RepositoryBase(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string ConnectionString
        {
            set
            {
                _connectionString = value;
            }
        }

        public DataSet FillDataSet(SqlCommand cmd, string tableName)
        {
            var con = new SqlConnection(_connectionString);
            cmd.Connection = con;

            var adapter = new SqlDataAdapter(cmd);

            var ds = new DataSet();

            try
            {
                con.Open();
                adapter.Fill(ds, tableName);
            }
            finally
            {
                con.Close();
            }

            return ds;
        }
    }
}
