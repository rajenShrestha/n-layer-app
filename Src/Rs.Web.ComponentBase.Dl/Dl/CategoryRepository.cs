﻿using Rs.Web.ComponentBase.Models;
/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/23/2015 11:33:23 PM
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rs.Web.ComponentBase.Dl
{
    public class CategoryRepository : RepositoryBase, IRepository<Category>
    {
        public static readonly string TableName = "Categories";
        public static readonly string IdColumn = "Id";
        public static readonly string NameColumn = "Name";

        public CategoryRepository()
        {

        }

        public CategoryRepository(string connectionString)
            : base(connectionString)
        {
            
        }

        public List<Category> GetAll()
        {
            var categories = new List<Category>();
            string query = "Select * from " + TableName;
            SqlCommand cmd = new SqlCommand(query);
            var categoriesDataSet = FillDataSet(cmd, TableName);
            categories = CreateCategoryList(categoriesDataSet);
            return categories;
        }

        //this is return only one, always, FIX IT
        public List<Category> Get(long Id)
        {
            var categories = new List<Category>();
            string query = "Select * from categories where Id=" + Id;
            SqlCommand cmd = new SqlCommand(query);
            var categoriesDataSet = FillDataSet(cmd, TableName);
            categories = CreateCategoryList(categoriesDataSet);
            return categories;
        }

        private List<Category> CreateCategoryList(System.Data.DataSet dataSet)
        {
            var categories = new List<Category>();
            foreach (DataRow dr in dataSet.Tables[TableName].Rows)
            {
                categories.Add(CreateCategory(dr));
            }

            return categories;
        }

        private Category CreateCategory(DataRow dataRow)
        {
            var category = new Category {
                Id = (long)dataRow[IdColumn],
                Name = dataRow[NameColumn] as string
            };
            return category;
        }

        public void Add(Category value)
        {
            throw new NotImplementedException();
        }


        public void Update(Category value)
        {
            throw new NotImplementedException();
        }
    }
}
