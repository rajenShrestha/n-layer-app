﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RS.Web.ComponentBase.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#contactPageInfo").on("mouseover", function () {
                $("#contactPageInfo").popover({
                    
                    title: "Contact Page Info",
                    tigger: "hover",
                    html: true,
                    placement:"top",
                    content: '<div class="well"><p>Show how to use Google Map.</p><p>Show how to use form and form-controls</p></div>',
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="headBannder" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
    <div id="carouselSlider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carouselSlider" data-slide-to="0" class="active"></li>
            <li data-target="#carouselSlider" data-slide-to="1"></li>
            <li data-target="#carouselSlider" data-slide-to="2"></li>
            <li data-target="#carouselSlider" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/Images/DSC_0019.JPG" alt="..."/>
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item">
                <img src="/Images/DSC_0020.JPG" alt="..."/>
                <div class="carousel-caption">
                    Second Image
                </div>
            </div>
            <div class="item">
                <img src="/Images/DSC_0021.JPG" alt="..."/>
                <div class="carousel-caption">
                    Third Image
                </div>
            </div>
            <div class="item">
                <img src="/Images/DSC_0022.JPG" alt="..."/>
                <div class="carousel-caption">
                    Fourth Image
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carouselSlider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carouselSlider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <a class="btn btn-default" data-toggle="collapse" href="#collapseProductPageInfo" aria-expanded="true" aria-controls="collapseNotes">Product page Info
    </a>
    <a id="contactPageInfo" class="btn btn-default" data-toggle="collapse" href="#collapseContactPageInfo" aria-expanded="true" aria-controls="collapseGoogleMap">Contact Page Info
    </a>
    <div class="collapse" id="collapseProductPageInfo">
        <div class="well">
            <p>Show how to use ObjectDataSource => chick on the product on menu bar.</p>
            <p>The page uses AJAX control</p>
            <ul>
                <li>Script Mananger</li>
                <li>UpdatePanel</li>
                <li>UpdateProgress</li>
            </ul>
        </div>
    </div>

    <div class="collapse" id="collapseContactPageInfo">
        <div class="well">
            <p>Show how to use Google Map.</p>
            <p>Show how to use form and form-controls</p>
        </div>
    </div>

</asp:Content>
