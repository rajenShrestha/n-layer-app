﻿using Rs.Web.ComponentBase.Dl;
/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/24/2015 5:43:58 PM
*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RS.Web.ComponentBase.Repository
{
    public class CategoryProxyRepository: CategoryRepository
    {
        public CategoryProxyRepository()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
            ConnectionString = connectionString;
        }
        
    }
}