﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace RS.Web.ComponentBase
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }


        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            // Get the exception object.
            Exception exc = Server.GetLastError();

            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpException))
            {
                //Redirect HTTP errors to HttpError page
                Server.Transfer("~\\ErrorPages\\HttpErrorPage.aspx");
            }
            else
            {
                Server.Transfer("~\\ErrorPages\\GenericErrorPage.aspx");
            }
                       
            // Clear the error from the server
            Server.ClearError();
        }

    }
}