﻿<%@ Page Title="Error Page" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="GenericErrorPage.aspx.cs" Inherits="RS.Web.ComponentBase.ErrorPages.GenericErrorPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div>
        <h2>Error Page</h2>
        <asp:Panel ID="InnerErrorPanel" runat="server" Visible="false">
            <p>
                Inner Error Message:<br />
                <asp:Label ID="innerMessage" runat="server" Font-Bold="true"
                    Font-Size="Large" /><br />
            </p>
            <pre>
        <asp:Label ID="innerTrace" runat="server" />
      </pre>
        </asp:Panel>
        <p>
            Error Message:<br />
            <asp:Label ID="exMessage" runat="server" Font-Bold="true"
                Font-Size="Large" />
        </p>
        <pre>
      <asp:Label ID="exTrace" runat="server" Visible="false" />
    </pre>
        <br />
        Return to the <a href='/Default.aspx'>Default Page</a>
    </div>
</asp:Content>
