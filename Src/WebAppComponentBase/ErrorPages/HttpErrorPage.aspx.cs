﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RS.Web.ComponentBase.ErrorPages
{
    /// <summary>
    /// The page just show the message to user.
    /// This page does not log error to system. The Elmah will log error to database. Pleae check the web.config for that.
    /// If you want to send error notification to Admin, then set the email notification on Elmah. http://elmah.googlecode.com/svn/tags/REL-1.2-SP1/samples/web.config for info
    /// </summary>
    public partial class HttpErrorPage : System.Web.UI.Page
    {
        protected HttpException ex = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            ex = (HttpException)Server.GetLastError();
            int httpCode = ex.GetHttpCode();

            // Filter for Error Codes and set text
            if (httpCode >= 400 && httpCode < 500)
                ex = new HttpException
                    (httpCode, "Not able to process client request.", ex);
            else if (httpCode > 499)
                ex = new HttpException
                    (ex.ErrorCode, "Error has been encountered in web server", ex);
            else
                ex = new HttpException
                    (httpCode, "Unexpected error has been encountered", ex);

            // Fill the page fields
            exMessage.Text = string.Format("Http {0}: {1}", httpCode,ex.Message);
            exTrace.Text = ex.StackTrace;

            // Show Inner Exception fields for local access
            if (ex.InnerException != null)
            {
                innerTrace.Text = ex.InnerException.StackTrace;
                InnerErrorPanel.Visible = Request.IsLocal;
                innerMessage.Text = string.Format("HTTP {0}: {1}",
                  httpCode, ex.InnerException.Message);
            }
            // Show Trace for local access
            exTrace.Visible = Request.IsLocal;

            // Clear the error from the server
            Server.ClearError();
        }
    }
}