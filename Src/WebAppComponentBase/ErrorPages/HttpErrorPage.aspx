﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="HttpErrorPage.aspx.cs" Inherits="RS.Web.ComponentBase.ErrorPages.HttpErrorPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">

    <div>
        <h2>Http Error Page</h2>
        <!-- This panel will be display if the server is local -->
        <asp:Panel ID="InnerErrorPanel" runat="server" Visible="false">
            <asp:Label ID="innerMessage" runat="server" Font-Bold="true"
                Font-Size="Large" /><br />
            <pre>
            <asp:Label ID="innerTrace" runat="server" />
            </pre>
        </asp:Panel>
        <!-- Error for client -->
        Error Message:<br />
        <asp:Label ID="exMessage" runat="server" Font-Bold="true"
            Font-Size="Large" />
        <pre>
      <asp:Label ID="exTrace" runat="server" Visible="false" />   
    </pre>
        <br />
        Return to the <a href='/Default.aspx'>Default Page</a>
    </div>
</asp:Content>
