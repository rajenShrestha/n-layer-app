﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RS.Web.ComponentBase.ErrorPages
{
    /// <summary>
    /// The page just show the message to user.
    /// This page does not log error to system. The Elmah will log error to database. Pleae check the web.config for that.
    /// If you want to send error notification to Admin, then set the email notification on Elmah
    /// </summary>
    public partial class GenericErrorPage : System.Web.UI.Page
    {
        protected Exception ex = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the last error from the server
            Exception ex = Server.GetLastError();

            // Create a safe message
            string safeMsg = "A problem has occurred in the web site. ";

            // Show Inner Exception fields for local access
            if (ex.InnerException != null)
            {
                innerTrace.Text = ex.InnerException.StackTrace;
                InnerErrorPanel.Visible = Request.IsLocal;
                innerMessage.Text = ex.InnerException.Message;
            }
            // Show Trace for local access
            if (Request.IsLocal)
                exTrace.Visible = true;
            else
                ex = new Exception(safeMsg, ex);

            // Fill the page fields
            exMessage.Text = ex.Message;
            exTrace.Text = ex.StackTrace;

            // Clear the error from the server
            Server.ClearError();
        }
    }
}