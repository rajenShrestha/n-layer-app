﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="RS.Web.ComponentBase.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="/Scripts/appGoogleMap.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
    <div id="map-canvas">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="alert alert-info">
        <h6><i class="glyphicon glyphicon-info-sign">&nbsp;</i>Please use following form to send your queries with your contact number or emaill address</h6>
    </div>
    <div class="row">
        <div class="form-group col-md-8">
            <label for="nameInputEmail1">Name</label>
            <input type="text" class="form-control" id="nameInputEmail1" placeholder="Name">
            <label for="lNameInputEmail1">Name</label>
            <input type="text" class="form-control" id="lNameInputEmail1" placeholder="Last Name">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
            <label for="messageInputEmail1">Message/Query</label>
            <textarea cols="20" rows="5" class="form-control" id="messageInputEmail1" placeholder="Message/Query"></textarea>
            <input id="SendButton" type="submit" value="Send" class="btn btn-group-lg btn-primary " />
        </div>
        <div class="col-md-4 alert-danger">
            <h5>Contact Details</h5>
            <p>Mail Address: </p>
            <p>12-14 Neild Ave Darlinghurst NSW Australia 2010</p>
            <p>Contact Number: +61 2 411629849</p>
        </div>
    </div>


</asp:Content>

