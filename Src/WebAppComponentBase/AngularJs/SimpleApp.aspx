﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="SimpleApp.aspx.cs" Inherits="RS.Web.ComponentBase.AngularJs.SimpleApp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/AngularJs/simpleApplication.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
    <h3>Simple Application</h3>
    <div class="alert-info">
        <blockquote>
            <p>For more: <a href="https://docs.angularjs.org/api/ng/filter">Filters</a></p>
        </blockquote>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="alert-info">
        <p>In this page, I have done simple application that uses controller and AngurlarJs Module</p>
        <p>Please check the source code of the page.</p>
        <ul>
            <li>use of model: ng-model</li>
            <li>use of ng-controller</li>
            <li>use of ng-repeat</li>
            <li>use of filter like currency, orderBy, uppercase</li>
        </ul>

    </div>
    <div class="row" ng-app="simpleApplication" ng-controller="simpleApplicationController">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="inputFirstName">First Name:</label>
                <input id="inputFirstName" type="text" role="textbox" name="firstName"
                    class="form-control" ng-model="firstName" />
            </div>

            <div class="form-group">
                <label for="inputLastName">LastName:</label>
                <input id="inputLastName" type="text" role="textbox" name="lastName"
                    class="form-control" ng-model="lastName" />
            </div>

            <div class="form-group">
                <label for="inputChildNameFilter">Filter Child:</label>
                <input id="inputChildNameFilter" type="text" role="textbox"
                    class="form-control" ng-model="childName" placeholder="Type child name to filter the list below" />
            </div>
            <ul>
                <li ng-repeat="child in children | filter:childName | orderBy:'age'">
                    {{(child.name | uppercase) + " age is " + child.age}}
                    <br />  Date of birth: {{child.dob | date:'dd-MM-yyyy'}}
                    <br />  Date of birth: {{child.dob | date:'longDate'}}
                </li>
            </ul>
            <div class="form-group">
                <!-- Angular Js Expression -->
                Full Name: <strong>{{fullName() | uppercase}}</strong>  {{ + " " + expense | currency}}   
            </div>
        </div>
    </div>

    <blockquote class="alert-info">
        <p>module defines Angular Js Application</p>
        <p>controller controls Angular Js Application</p>
        <code>"ng-controller" directive is used in this application
        </code>
        <br />
        <code>In javascript, angular.module and controller is used           
        </code>
        <pre>
                var app = angular.module("simpleApplication", []);
                app.controller("simpleApplicationController", function ($scope) {
                $scope.firstName = "Rajen";
                $scope.lastName = "Shrestha";
                });
            </pre>
    </blockquote>

</asp:Content>
