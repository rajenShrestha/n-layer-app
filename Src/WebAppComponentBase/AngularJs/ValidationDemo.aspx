﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="ValidationDemo.aspx.cs" Inherits="RS.Web.ComponentBase.AngularJs.ValidationDemo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <div class="row">
        <form></form>
        <form ng-app="validationDemoApp" ng-controller="validationDemoAppController" class="form-horizontal"
            name="myForm" novalidate>

            <p>
                Username:<br>
                <input type="text" name="user" ng-model="user" class="form-control" required>
                <span style="color: red" ng-show="myForm.user.$dirty && myForm.user.$invalid">
                    <span ng-show="myForm.user.$error.required">Username is required.</span>
                </span>
            </p>

            <p>
                Email:<br>
                <input type="email" name="email" ng-model="email" class="form-control" required>
                <span style="color: red" ng-show="myForm.email.$dirty && myForm.email.$invalid">
                    <span ng-show="myForm.email.$error.required">Email is required.</span>
                    <span ng-show="myForm.email.$error.email">Invalid email address.</span>
                </span>
            </p>

            <p>
                <input type="submit" class="btn btn-primary btn-lg" ng-click="concat()"
                    ng-disabled="myForm.user.$dirty && myForm.user.$invalid ||
  myForm.email.$dirty && myForm.email.$invalid">
            </p>

            <p>
                <span ng-show="myForm.$submitted">
                    Your name and email address is {{nameAndEmailAddress}}
                </span>
            </p>

        </form>

    </div>
    <script src="/AngularJs/validationDemoController.js"></script>
    <blockquote class="alert alert-info">
        <p>For more info: <a href="https://docs.angularjs.org/api/ng/type/form.FormController">$error and form validation</a></p>
    </blockquote>
</asp:Content>
