﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RS.Web.ComponentBase.AngularJs.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <h3>Angular Js: Introduction</h3>
    <blockquote class="alert-success">
        <p>Angular <a href="http://www.w3schools.com/angular/angular_ref_directives.asp">Reference</a></p>
        <footer>-W3 School</footer>
    </blockquote>

    <div class="row" ng-app>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="inputName">Name:</label>
                <input id="inputName" type="text" role="textbox"
                    class="form-control" ng-model="name" />
            </div>
            <br />
            <blockquote class="alert-info">
                <h6>AngularJS directives are HTML attributes with an ng prefix</h6>
                <code>eg: ng-model="name" And ng-bind="name And ng-init='shrestha'
                </code>
                <p>For more: <a href="https://docs.angularjs.org/api/ng/directive">directives</a></p>
            </blockquote>
            <div class="form-group">
                <div ng-init="lastname='shrestha'" />
                <div class="form-group">
                    <label for="inputLastName">Last Name:</label>
                    <input id="inputLastName" type="text" role="textbox"
                        class="form-control" ng-model="lastname" />
                </div>
            </div>

            <div class="form-group">
                <label id="labelMessage">{{name}} {{lastname}}</label>
            </div>
        </div>
    </div>

</asp:Content>
