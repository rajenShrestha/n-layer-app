﻿var app = angular.module("validationDemoApp", []);

app.controller("validationDemoAppController", function ($scope) {
    $scope.user = 'Rajen Shrestha';
    $scope.email = 'rajen.shrestha@gmail.com';

    $scope.concat = function () {
        $scope.nameAndEmailAddress = $scope.user + " " + $scope.email
    };
});