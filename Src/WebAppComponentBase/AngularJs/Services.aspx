﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppMaster.Master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="RS.Web.ComponentBase.AngularJs.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/AngularJs/servicesController.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeaderContentPlace" runat="server">
    <h3>Service</h3>
    <div class="alert-info">
        <blockquote>
            <p>For more: <a href="https://docs.angularjs.org/api/ng/service">Services</a></p>
        </blockquote>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContentPlaceHolder" runat="server">
    <h3>List of Countries:</h3>

    <div class="row" ng-app="serviceApp" ng-controller="serviceAppController">
        <div class="form-group">
            <label for="inputCountryName">Filter by Country Name:</label>
            <input id="inputCountryName" name="countryName" ng-model="countryName" role="textbox" />
        </div>
        <ul>
            <li ng-repeat="country in countries | filter:countryName | orderBy:'Country'">
                Name: {{country.Name}}
                <br />
                City: {{country.City}}
                <br />
                Country: {{country.Country | uppercase}}<br />
            </li>
        </ul>
    </div>
</asp:Content>
