﻿var app = angular.module("serviceApp", []);

app.controller("serviceAppController", function ($scope, $http) {
    
    $http.get("/Services/CountriesList.txt")
        .success(function (data, status, headers, config) {
            $scope.countries = data;
        })
        .error(function (data, status, headers, config) {
            $scope.error = "Error: " + status + " error has encountered";
        })

});