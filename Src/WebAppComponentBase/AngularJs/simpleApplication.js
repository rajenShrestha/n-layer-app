﻿var app = angular.module("simpleApplication", []);

app.controller("simpleApplicationController", function ($scope) {
    $scope.firstName = "Rajen";
    $scope.lastName = "Shrestha";
    $scope.children = [
        {
            name: "Krish",
            age: 5,
            dob: new Date('2010', '07', '08')
        },
        {
            name: "Radhika",
            age: 8,
            dob: new Date("2007", "07", "15")
        }
    ];

    $scope.fullName = function () {
        return $scope.firstName + " " + $scope.lastName;
    };

    $scope.expense = 350.50;
});