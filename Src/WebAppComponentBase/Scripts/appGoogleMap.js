﻿$(document).ready(function () {
    var mapInitialiser = function () {
        var latlog = new google.maps.LatLng(-33.8770363, 151.2304145);

        var mapCanvas = $("#map-canvas").get(0);
        var mapOptions = {
            center: latlog,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var mapMarker = new google.maps.Marker({
            position: latlog,
            map: map,
            title: "Home - Rushcutters Bay",
        });
        mapMarker.setAnimation(google.maps.Animation.DROP);

    };

    mapInitialiser();
});